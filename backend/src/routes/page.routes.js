const router = require('express').Router();
const controller = require('../controllers/page.controller');
const { verify } = require('../middlewares/auth');

// get
router.get('/', controller.get);
router.get('/:id', controller.getById);
router.get('/get/actives', controller.getActives);

// post
router.post('/', verify, controller.create);

// put
router.put('/:id', verify, controller.update);
router.put('/activate/:id', verify, controller.activate);
router.put('/deactivate/:id', verify, controller.deactivate);

// delete
router.delete('/:id', verify, controller.destroy);


// #######################################################################    CUSTOM


module.exports = router;