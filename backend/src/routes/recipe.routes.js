const router = require('express').Router();
const controller = require('../controllers/recipe.controller');
const { verify } = require('../middlewares/auth');


const multer = require('multer');
const path = require('path');
const {v4: uuidv4} = require('uuid');


const getFrm = (req, res, next) => {
    const images = req.body;
    console.log('images', images);
    next()
}


const storage = multer.diskStorage({
    filename: function(req, file, cb) {
        file.finalFilename = file.fieldname + '_' +  uuidv4() + path.extname(file.originalname).toLocaleLowerCase();
        cb(null, file.finalFilename);
    },
    destination: path.join(__dirname, '../files'),
});


const upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb){
        const fileTypes = /jpeg|jpg|png/;
        const mimetype = fileTypes.test(file.mimetype);
        const extname = fileTypes.test(path.extname(file.originalname));
        if (mimetype && extname){
            return cb(null, true);
        }
        cb('ERROR: el archivo debe ser tipo ".jpeg, .jpg o .png"!')
    }
}).array('images');




// get
router.get('/', controller.get);
router.get('/:id', controller.getById);
router.get('/get/actives', controller.getActives);

// post
router.post('/', verify, controller.create);

// put
router.put('/:id', verify, controller.update);
router.put('/activate/:id', verify, controller.activate);
router.put('/deactivate/:id', verify, controller.deactivate);

// delete
router.delete('/:id', verify, controller.destroy);


// #######################################################################    CUSTOM


// get
router.get('/getCompleteRecipe/:id', controller.getCompleteRecipe);
router.get('/get/byAuthor/:id', controller.getByAuthor);
router.get('/get/topTen', controller.getTopTen);
router.get('/get/myRecipes', verify, controller.getMyRecipes);
router.get('/get/myRecipes/private', verify, controller.getMyRecipesPrivate);

// post
router.post('/createRecipe', verify, controller.createRecipe);


router.post('/uploadImages', verify, getFrm, upload, async (req, res) => {
    let imgs = req.files;
    const response = req.files.map(item => {
        return { name: item.fieldname, description: item.destination, url: item.filename, path: item.path}
    });
    console.log(imgs);
    res.json(response);
});


module.exports = router;