const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const { urlencoded } = require('express');

const path = require('path');



// init
const app = express();



// settings
app.set('port', process.env.PORT || 4000);



// middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(urlencoded({ extended: false }));



// routes
app.use('/api/auth', require('./routes/auth.routes'));
app.use('/api/users', require('./routes/user.routes'));
app.use('/api/pages', require('./routes/page.routes'));
app.use('/api/recipes', require('./routes/recipe.routes'));
app.use('/api/images', require('./routes/image.routes'));
app.use('/api/ingredients', require('./routes/ingredient.routes'));
app.use('/api/qualifications', require('./routes/qualification.routes'));

app.use('/files', express.static( path.join(__dirname, './files')));



// run
app.listen(app.get('port'), () => {
    console.log('server on port: ', app.get('port'));
});
