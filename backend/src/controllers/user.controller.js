const users = require('../database/users.json');
const fs = require('fs');
const Str = require('@supercharge/strings');
const moment = require('moment');
const jwt = require('jsonwebtoken');


class userController{
    
    constructor (){

    }

    async create(req, res){
        const { names, lastnames, username, email, password } = req.body;
        if (names && lastnames && username && email && password){
            const id = users.length + 1;
            const user = {id, ...req.body, status: 'A'}
            users.push(user);
            fs.writeFileSync('src/database/users.json', JSON.stringify(users), 'utf-8');
            res.json(user);
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async get(req, res){
        const response = users.filter((item) => {
            return item.status != 'E';
        });
        res.json(response);
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        const filter = users.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        const response = filter.length > 0? filter[0]: {};
        res.json(response);
    }

    async getActives(req, res){
        const response = users.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { names, lastnames, username, email, password } = req.body;

        if (names && lastnames && username && email && password){
            const index = users.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                users[index].names = names;
                users[index].lastnames = lastnames;
                users[index].username = username;
                users[index].email = email;
                users[index].password = password;

                fs.writeFileSync('src/database/users.json', JSON.stringify(users), 'utf-8');

                res.json(users[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'el usuario no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = users.findIndex(item => item.id == id);

        if (index >= 0){
            users[index].status = 'E';

            fs.writeFileSync('src/database/users.json', JSON.stringify(users), 'utf-8');

            res.json(users[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = users.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            users[index].status = 'A';

            fs.writeFileSync('src/database/users.json', JSON.stringify(users), 'utf-8');

            res.json(users[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    async deactivate(req, res){

        const id = parseInt(req.params.id);
        const index = users.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            users[index].status = 'D';

            fs.writeFileSync('src/database/users.json', JSON.stringify(users), 'utf-8');

            res.json(users[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

    async login(req, res) {
        const { email, password } = req.body;
        const filter = users.filter((item) => {
            return item.email == email && item.password == password && item.status == 'A';
        });
        if (filter.length > 0){
            const user = filter[0];

            jwt.sign({id: user.id}, 'secret', (err, token) => {
                res.json({token});
            });

        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

}

const c = new userController();

module.exports = c;