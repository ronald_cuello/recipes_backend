const images = require('../database/images.json');
const fs = require('fs');

class imageController{
    
    constructor (){

    }

    async create(req, res){
        const { name, description, url, recipe_id } = req.body;
        if (name && description && url && recipe_id){
            const id = images.length + 1;
            const image = {id, ...req.body, status: 'A'}
            images.push(image);
            fs.writeFileSync('src/database/images.json', JSON.stringify(images), 'utf-8');
            res.json(image);
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async get(req, res){
        const response = images.filter((item) => {
            return item.status != 'E';
        });
        res.json(response);
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        const filter = images.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        const response = filter.length > 0? filter[0]: {};
        res.json(response);
    }

    async getActives(req, res){
        const response = images.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { name, description, url, recipe_id } = req.body;

        if (name && description && url && recipe_id){
            const index = images.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                images[index].name = name;
                images[index].description = description;
                images[index].url = url;
                images[index].recipe_id = recipe_id;

                fs.writeFileSync('src/database/images.json', JSON.stringify(images), 'utf-8');

                res.json(images[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'el usuario no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = images.findIndex(item => item.id == id);

        if (index >= 0){
            images[index].status = 'E';

            fs.writeFileSync('src/database/images.json', JSON.stringify(images), 'utf-8');

            res.json(images[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = images.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            images[index].status = 'A';

            fs.writeFileSync('src/database/images.json', JSON.stringify(images), 'utf-8');

            res.json(images[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    async deactivate(req, res){
        const id = parseInt(req.params.id);
        const index = images.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            images[index].status = 'D';

            fs.writeFileSync('src/database/images.json', JSON.stringify(images), 'utf-8');

            res.json(images[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el usuario no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

}

const c = new imageController();

module.exports = c;