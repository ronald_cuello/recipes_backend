const recipes = require('../database/recipes.json');
const ingredients_db = require('../database/ingredients.json');
const images_db = require('../database/images.json');
const users_db = require('../database/users.json');
const qualifications_db = require('../database/qualifications.json');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const { response } = require('express');

class recipeController{
    
    constructor (){

    }

    async create(req, res){
        const { name, description, preparation, note } = req.body;
        if (name && description && preparation && note){
            const id = recipes.length + 1;
            const recipe = {id, ...req.body, status: 'A'}
            recipes.push(recipe);
            fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');
            res.json(recipe);
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async get(req, res){
        let all = [];
        const list = recipes;

        const response = list.filter((item) => {
            return item.status != 'E';
        });
        
        for (let i = 0; i < response.length; i++) {
            const author = users_db.filter((item) => {
                return item.id == recipes[i].author_id && item.status != 'A';
            });
            all.push({...response[i], author: {...author[0]}})
        }
        res.json(all);
        
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        let r = {};

        // let response = [];

        const filter = recipes.filter((item) => {
            return item.id == id && item.status == 'A';
        });

        if (filter.length > 0){
            r = filter[0];

            const ingredients = ingredients_db.filter((item) => {
                return item.recipe_id == r.id && item.status == 'A';
            });
    
            const images = images_db.filter((item) => {
                return item.recipe_id == r.id && item.status == 'A';
            });
    
            // const img = images.length > 0? images: [{url: 'default.jpg'}];
    
            r = {...r, images, ingredients} ;



        }

        

        res.json(r);
    }

    async getActives(req, res){
        const response = recipes.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { name, description, preparation, note } = req.body;

        if (name && description && preparation && note){
            const index = recipes.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                recipes[index].name = name;
                recipes[index].description = description;
                recipes[index].preparation = preparation;
                recipes[index].note = note;

                fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');

                res.json(recipes[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'la receta no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = recipes.findIndex(item => item.id == id);

        if (index >= 0){
            recipes[index].status = 'E';

            fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');

            res.json(recipes[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la receta no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = recipes.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            recipes[index].status = 'A';

            fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');

            res.json(recipes[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la receta no existe'
            });
        }
    }

    async deactivate(req, res){
        const id = parseInt(req.params.id);
        const index = recipes.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            recipes[index].status = 'D';

            fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');

            res.json(recipes[index]);
        }else{
            res.status(200).json({
                message: 'la receta no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

    async getCompleteRecipe(req, res){

        const list = recipes;
        const id = parseInt(req.params.id);

        const filter = list.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        if (filter.length > 0){
            const filter_ingredients = ingredients_db.filter((item) => {
                return item.recipe_id == id && item.status == 'A';
            });
            const filter_images = images_db.filter((item) => {
                return item.recipe_id == id && item.status == 'A';
            });
            const response = filter[0];

            res.json({...response, ingredients: filter_ingredients, images: filter_images});
        }else{
            res.status(200).json({
                message: 'la receta no existe'
            });
        }
    }

    
    async getByAuthor(req, res){
        const author = req.params.id;
        const response = recipes.filter((item) => {
            return item.author_id == author && item.status != 'E';
        });
        res.json(response);
    }


    async createRecipe(req, res){

        const me = jwt.verify(req.token, 'secret');
        const { name, description, preparation, note, ingredients, images } = req.body;
        if (name && description && preparation && note){
            const id = recipes.length + 1;
            const recipe = {id, name, description, preparation, note, author_id: me.id, status: 'A'}
            recipes.push(recipe);
            fs.writeFileSync('src/database/recipes.json', JSON.stringify(recipes), 'utf-8');

            for (let i = 0; i < ingredients.length; i++) {
                const ingredient_id = ingredients_db.length + 1;
                const data = {ingredient_id, ...ingredients[i], recipe_id: id, status: 'A'}
                ingredients_db.push(data);
            }
            fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients_db), 'utf-8');


            for (let i = 0; i < images.length; i++) {
                const image_id = images_db.length + 1;
                const data = {image_id, ...images[i], recipe_id: id, status: 'A'}
                images_db.push(data);
            }
            fs.writeFileSync('src/database/images.json', JSON.stringify(images_db), 'utf-8');
            res.json(recipe);
        }else{
            res.status(500).json({
                message: 'datos incompletos'
            });
        }
    }

    async getTopTen(req, res) {
        var averages = [];
        var list = recipes;
        const all = list.filter((item) => {
            return item.status == 'A';
        });

        console.log(all);

        for (let i = 0; i < all.length; i++) {
            var q = qualifications_db.filter((item) => {
                return item.recipe_id == all[i].id && item.status == 'A';
            });

            const author = users_db.filter((item) => {
                return item.id == all[i].author_id && item.status == 'A';
            });

            const images = images_db.filter((item) => {
                return item.recipe_id == all[i].id && item.status == 'A';
            });

            const img = images.length > 0? images: [{url: 'default.jpg'}];

            var E = 0;
            for (let i = 0; i < q.length; i++) {
                E += q[i].rate;
            }
            const average = E / q.length;
            averages.push({...all[i], average, author:{...author[0]}, images: img});
        }
        const top = averages.sort( (a, b) => { return b.average - a.average }).slice(0, 10);
        
        res.json(top);
    }

    async getMyRecipes(req, res){
        const me = jwt.verify(req.token, 'secret');
        const response = recipes.filter((item) => {
            return item.author_id == me.id && item.status == 'A';
        });

        res.json(response);
    }

    async getMyRecipesPrivate(req, res){
        const me = jwt.verify(req.token, 'secret');
        const response = recipes.filter((item) => {
            return item.author_id == me.id && item.status != 'E';
        });

        res.json(response);
    }

}

const c = new recipeController();

module.exports = c;