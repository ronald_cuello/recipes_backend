const qualifications = require('../database/qualifications.json');
const fs = require('fs');
const jwt = require('jsonwebtoken');

class qualificationController{
    
    constructor (){

    }

    async create(req, res){
        const me = jwt.verify(req.token, 'secret');
        const { rate, recipe_id } = req.body;
        
        const response = qualifications.filter((item) => {
            return item.user_id == me.id && item.recipe_id == recipe_id && item.status == 'A';
        });

        if (response.length > 0){
            const index = qualifications.findIndex(item => item.user_id == me.id && item.recipe_id == recipe_id && item.status == 'A');
            if (index >= 0){
                qualifications[index].rate = rate;
                fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');
                res.json(qualifications[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'la calificación no existe'
                });
            }
        }else{
            if (rate && recipe_id){
                const id = qualifications.length + 1;
                const qualification = {id, rate, user_id: me.id, recipe_id, status: 'A'}
                qualifications.push(qualification);
                fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');
                res.json(qualification);
            }else{
                res.json({
                    status: 500,
                    msg: 'datos incompletos'
                });
            }
        }


        
    }

    async get(req, res){
        const response = qualifications.filter((item) => {
            return item.status != 'E';
        });
        res.json(response);
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        const filter = qualifications.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        const response = filter.length > 0? filter[0]: {};
        res.json(response);
    }

    async getActives(req, res){
        const response = qualifications.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { rate } = req.body;

        if (rate){
            const index = qualifications.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                qualifications[index].rate = rate;

                fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');

                res.json(qualifications[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'la calificación no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = qualifications.findIndex(item => item.id == id);

        if (index >= 0){
            qualifications[index].status = 'E';

            fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');

            res.json(qualifications[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la calificación no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = qualifications.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            qualifications[index].status = 'A';

            fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');

            res.json(qualifications[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la calificación no existe'
            });
        }
    }

    async deactivate(req, res){
        const id = parseInt(req.params.id);
        const index = qualifications.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            qualifications[index].status = 'D';

            fs.writeFileSync('src/database/qualifications.json', JSON.stringify(qualifications), 'utf-8');

            res.json(qualifications[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la calificación no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

}

const c = new qualificationController();

module.exports = c;