const ingredients = require('../database/ingredients.json');
const fs = require('fs');

class ingredientController{
    
    constructor (){

    }

    async create(req, res){
        const { name, description, recipe_id } = req.body;
        if (name && description && recipe_id){
            const id = ingredients.length + 1;
            const ingredient = {id, ...req.body, status: 'A'}
            ingredients.push(ingredient);
            fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients), 'utf-8');
            res.json(ingredient);
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async get(req, res){
        const response = ingredients.filter((item) => {
            return item.status != 'E';
        });
        res.json(response);
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        const filter = ingredients.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        const response = filter.length > 0? filter[0]: {};
        res.json(response);
    }

    async getActives(req, res){
        const response = ingredients.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { name, description, recipe_id } = req.body;

        if (name && description && recipe_id){
            const index = ingredients.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                ingredients[index].name = name;
                ingredients[index].description = description;
                ingredients[index].recipe_id = recipe_id;

                fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients), 'utf-8');

                res.json(ingredients[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'el ingrediente no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = ingredients.findIndex(item => item.id == id);

        if (index >= 0){
            ingredients[index].status = 'E';

            fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients), 'utf-8');

            res.json(ingredients[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el ingrediente no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = ingredients.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            ingredients[index].status = 'A';

            fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients), 'utf-8');

            res.json(ingredients[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el ingrediente no existe'
            });
        }
    }

    async deactivate(req, res){
        const id = parseInt(req.params.id);
        const index = ingredients.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            ingredients[index].status = 'D';

            fs.writeFileSync('src/database/ingredients.json', JSON.stringify(ingredients), 'utf-8');

            res.json(ingredients[index]);
        }else{
            res.json({
                status: 500,
                msg: 'el ingrediente no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

}

const c = new ingredientController();

module.exports = c;