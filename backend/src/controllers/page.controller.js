const pages = require('../database/pages.json');
const fs = require('fs');

class pageController{
    
    constructor (){

    }

    async create(req, res){
        const { name, position, place, auth, url } = req.body;
        if (name && position && place && auth && url){
            const id = pages.length + 1;
            const page = {id, ...req.body, status: 'A'}
            pages.push(page);
            fs.writeFileSync('src/database/pages.json', JSON.stringify(pages), 'utf-8');
            res.json(page);
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async get(req, res){
        const response = pages.filter((item) => {
            return item.status != 'E';
        });
        res.json(response);
    }

    async getById(req, res){
        const id = parseInt(req.params.id);
        const filter = pages.filter((item) => {
            return item.id == id && item.status == 'A';
        });
        const response = filter.length > 0? filter[0]: {};
        res.json(response);
    }

    async getActives(req, res){
        const response = pages.filter((item) => {
            return item.status == 'A';
        });
        res.json(response);
    }

    async update(req, res){
        const id = parseInt(req.params.id);
        const { name, position, place, auth, url } = req.body;

        if (name && position && place && auth && url){
            const index = pages.findIndex(item => item.id == id && item.status == 'A');

            if (index >= 0){
                
                pages[index].name = name;
                pages[index].position = position;
                pages[index].place = place;
                pages[index].auth = auth;
                pages[index].url = url;

                fs.writeFileSync('src/database/pages.json', JSON.stringify(pages), 'utf-8');

                res.json(pages[index]);
            }else{
                res.json({
                    status: 500,
                    msg: 'la pagina no existe'
                });
            }
        }else{
            res.json({
                status: 500,
                msg: 'datos incompletos'
            });
        }
    }

    async destroy(req, res){
        const id = parseInt(req.params.id);
        const index = pages.findIndex(item => item.id == id);

        if (index >= 0){
            pages[index].status = 'E';

            fs.writeFileSync('src/database/pages.json', JSON.stringify(pages), 'utf-8');

            res.json(pages[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la página no existe'
            });
        }
    }

    async activate(req, res){
        const id = parseInt(req.params.id);
        const index = pages.findIndex(item => item.id == id && item.status == 'D');

        if (index >= 0){
            pages[index].status = 'A';

            fs.writeFileSync('src/database/pages.json', JSON.stringify(pages), 'utf-8');

            res.json(pages[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la página no existe'
            });
        }
    }

    async deactivate(req, res){
        const id = parseInt(req.params.id);
        const index = pages.findIndex(item => item.id == id && item.status == 'A');

        if (index >= 0){
            pages[index].status = 'D';

            fs.writeFileSync('src/database/pages.json', JSON.stringify(pages), 'utf-8');

            res.json(pages[index]);
        }else{
            res.json({
                status: 500,
                msg: 'la página no existe'
            });
        }
    }

    // -------------------------------------------------------------------------------- custom

}

const c = new pageController();

module.exports = c;